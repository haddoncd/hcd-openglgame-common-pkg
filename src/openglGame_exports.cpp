#ifdef openglGame_exports_cpp
#error Multiple inclusion
#endif
#define openglGame_exports_cpp

#ifndef openglGame_exports_hpp
#include "openglGame_exports.hpp"
#endif

#ifndef assets_cpp
#error "Please include assets.cpp before this file"
#endif


extern "C" ZStr openglGame_COMMON_PKG_VER = ZSTR(OPENGLGAME_COMMON_PKG_VER);
extern "C" openglGame::AssetList openglGame_ASSET_LIST = { arrayCount(assets::labels), assets::labels };
