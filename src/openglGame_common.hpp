#ifdef openglGame_common_hpp
#error Multiple inclusion
#endif
#define openglGame_common_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef args_hpp
#error "Please include args.hpp before this file"
#endif

#ifndef HeapAllocator_hpp
#error "Please include HeapAllocator.hpp before this file"
#endif

#ifndef gl3w_Functions_hpp
#error "Please include gl3w_Functions.hpp before this file"
#endif



namespace openglGame
{
  // FIXME: ifdef debug

  #define openglGame_CLIPBOARD_COPY_FUNCTION(name) void name(const char *text)
  typedef openglGame_CLIPBOARD_COPY_FUNCTION(ClipboardCopyFunction);

  #define openglGame_CLIPBOARD_PASTE_FUNCTION(name) const char *name()
  typedef openglGame_CLIPBOARD_PASTE_FUNCTION(ClipboardPasteFunction);

  struct DebugResources
  {
    // Clipboard functions to be called from the main thread ONLY.
    struct
    {
      // Takes a copy of the pointed string before returning
      ClipboardCopyFunction *copy;
      // Returned pointer is valid until next clipboard call
      ClipboardPasteFunction *paste;
    } clipboard;
  };

  // forward decl only, include AssetLabel.hpp for def
  struct AssetLabel;

  struct AssetList
  {
    u64         count;
    AssetLabel *labels;
  };

  namespace AssetStatus
  {
    enum Enum
    {
      UNLOADED,
      LOADED,
      RELOADED,
      ENUM_COUNT
    };

    bool isLoaded[];
  }

  struct GameResources
  {
    gl3w::Functions     gl;

    HeapAllocator      *platformHeap;

    DebugResources      debugResources;

    void              **assets;
    AssetStatus::Enum  *assetStatuses;

    void               *state;
  };

  struct ButtonState
  {
    u32  transitionCount;
    bool isDown;

    bool wasPressed();
  };

  namespace MouseButton
  {
    enum Enum
    {
      LEFT,
      RIGHT,
      MIDDLE,
      ENUM_COUNT
    };
  }

  struct MouseState
  {
    ButtonState buttons[MouseButton::ENUM_COUNT];
    bool        cursorPresent;
    f32         scroll_wheelClicks;
    V2<f64>     cursorPos_points; // origin = bottom left, +x = right, +y = up
  };

  namespace KeyboardKey
  {
    enum Enum
    {
      SPACE,
      APOSTROPHE,
      COMMA,
      MINUS,
      PERIOD,
      SLASH,
      NUM_0,
      NUM_1,
      NUM_2,
      NUM_3,
      NUM_4,
      NUM_5,
      NUM_6,
      NUM_7,
      NUM_8,
      NUM_9,
      SEMICOLON,
      EQUAL,
      A,
      B,
      C,
      D,
      E,
      F,
      G,
      H,
      I,
      J,
      K,
      L,
      M,
      N,
      O,
      P,
      Q,
      R,
      S,
      T,
      U,
      V,
      W,
      X,
      Y,
      Z,
      LEFT_BRACKET,
      BACKSLASH,
      RIGHT_BRACKET,
      GRAVE_ACCENT,
      ESCAPE,
      ENTER,
      TAB,
      BACKSPACE,
      INSERT,
      DEL,
      RIGHT_ARROW,
      LEFT_ARROW,
      DOWN_ARROW,
      UP_ARROW,
      PAGE_UP,
      PAGE_DOWN,
      HOME,
      END,
      F1,
      F2,
      F3,
      F4,
      F5,
      F6,
      F7,
      F8,
      F9,
      F10,
      F11,
      F12,
      LEFT_SHIFT,
      LEFT_CONTROL,
      LEFT_ALT,
      RIGHT_SHIFT,
      RIGHT_CONTROL,
      RIGHT_ALT,
      ENUM_COUNT
    };
  }

  struct TextInput
  {
    u8  *bytes_utf8;
    bool overflowed;
  };

  struct KeyboardState
  {
    ButtonState keys[KeyboardKey::ENUM_COUNT];
    TextInput   text;
  };

  struct Input
  {
    V2<u32>       windowSize_pixels;
    V2<f64>       windowSize_points;
    MouseState    mouse;
    KeyboardState keyboard;
    f32           timeStep_seconds;
  };

  #define openglGame_LIB_INIT openglGame_libInit
  #define openglGame_LIB_INIT_FUNCTION(name) bool name(::openglGame::GameResources *resources)
  typedef openglGame_LIB_INIT_FUNCTION(LibInitFunction);

  #define openglGame_LIB_SHUTDOWN openglGame_libShutdown
  #define openglGame_LIB_SHUTDOWN_FUNCTION(name) void name(::openglGame::GameResources *resources)
  typedef openglGame_LIB_SHUTDOWN_FUNCTION(LibShutdownFunction);

  #define openglGame_GAME_INIT openglGame_gameInit
  #define openglGame_GAME_INIT_FUNCTION(name) bool name(::openglGame::GameResources *resources, ::args::Iterator args)
  typedef openglGame_GAME_INIT_FUNCTION(GameInitFunction);

  #define openglGame_GAME_UPDATE openglGame_gameUpdate
  #define openglGame_GAME_UPDATE_FUNCTION(name) void name(::openglGame::GameResources *resources, ::openglGame::Input *input)
  typedef openglGame_GAME_UPDATE_FUNCTION(GameUpdateFunction);
}
