#ifdef openglGame_exports_hpp
#error Multiple inclusion
#endif
#define openglGame_exports_hpp

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif


extern "C" ZStr openglGame_COMMON_PKG_VER;
extern "C" openglGame::AssetList openglGame_ASSET_LIST;
extern "C" openglGame_LIB_INIT_FUNCTION(openglGame_LIB_INIT);
extern "C" openglGame_LIB_SHUTDOWN_FUNCTION(openglGame_LIB_SHUTDOWN);
extern "C" openglGame_GAME_INIT_FUNCTION(openglGame_GAME_INIT);
extern "C" openglGame_GAME_UPDATE_FUNCTION(openglGame_GAME_UPDATE);
