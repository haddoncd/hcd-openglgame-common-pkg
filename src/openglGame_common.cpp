#ifdef openglGame_common_cpp
#error Multiple inclusion
#endif
#define openglGame_common_cpp

#ifndef openglGame_common_hpp
#include "openglGame_common.hpp"
#endif


namespace openglGame
{
  // FIXME: ifdef debug

  namespace AssetStatus
  {
    bool isLoaded[ENUM_COUNT] =
    {
      false,
      true,
      true
    };
  }

  bool ButtonState::wasPressed()
  {
    return transitionCount > 1 || (transitionCount == 1 && isDown);
  }
}
