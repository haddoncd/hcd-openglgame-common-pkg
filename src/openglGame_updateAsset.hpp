#ifdef openglGame_updateAsset_hpp
#error Multiple inclusion
#endif
#define openglGame_updateAsset_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif



namespace openglGame
{
  void updateAsset(AssetLabel         *label,
                   void              **asset,
                   AssetStatus::Enum  *status,
                   u64                *file_time);
}
