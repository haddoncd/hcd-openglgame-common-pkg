#ifdef openglGame_AssetLabel_hpp
#error Multiple inclusion
#endif
#define openglGame_AssetLabel_hpp

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef assetManager_config_assetType_hpp
#error "Please include assetManager_config_assetType.hpp before this file"
#endif


namespace openglGame
{
  struct AssetLabel
  {
    assetManager::config::assetType::Enum type;
    Str                                   name;
    Str                                   params;
  };
}
