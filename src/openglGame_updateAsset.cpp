#ifdef openglGame_updateAsset_cpp
#error Multiple inclusion
#endif
#define openglGame_updateAsset_cpp

#ifndef openglGame_updateAsset_hpp
#include "openglGame_updateAsset.hpp"
#endif

#ifndef File_hpp
#error "Please include File.hpp before this file"
#endif

#ifndef filesystem_hpp
#error "Please include filesystem.hpp before this file"
#endif

#ifndef HeapAllocator_hpp
#error "Please include HeapAllocator.hpp before this file"
#endif

#ifndef assetManager_config_assetType_cpp
#error "Please include assetManager_config_assetType.cpp before this file"
#endif

#ifndef openglGame_AssetLabel_hpp
#error "Please include openglGame_AssetLabel.hpp before this file"
#endif

#ifndef magicaVoxel_Palette_hpp
#error "Please include magicaVoxel_Palette.hpp before this file"
#endif

#ifndef magicaVoxel_File_hpp
#error "Please include magicaVoxel_File.hpp before this file"
#endif

#ifndef magicaVoxel_VoxelGrid_hpp
#error "Please include magicaVoxel_VoxelGrid.hpp before this file"
#endif

#ifndef magicaVoxel_Mesh_hpp
#error "Please include magicaVoxel_Mesh.hpp before this file"
#endif

#ifndef magicaVoxel_loadMesh_hpp
#error "Please include magicaVoxel_loadMesh.hpp before this file"
#endif



namespace openglGame
{
  void updateAsset(AssetLabel         *label,
                   void              **asset,
                   AssetStatus::Enum  *status,
                   u64                *file_time)
  {
    switch (label->type)
    {
      case assetManager::config::assetType::RAW_FILE:
      {
        ZStr path;
        {
          StrBuf::Variable *buf;
          sprintLocal(buf, label->params);
          path = buf->zStr();
        }

        if (!filesystem::exists(path))
        {
          if (AssetStatus::isLoaded[*status])
          {
            STD_HEAP->free(*asset);
            *asset = 0;
            *status = AssetStatus::UNLOADED;
            *file_time = 0;
          }
          break;
        }

        u64 file_last_write_time = filesystem::lastWriteTime(path);

        AssetStatus::Enum status_after_loading;

        if (!AssetStatus::isLoaded[*status])
        {
          status_after_loading = AssetStatus::LOADED;
        }
        else if (*file_time == file_last_write_time)
        {
          *status = AssetStatus::LOADED;
          break;
        }
        else
        {
          STD_HEAP->free(*asset);
          *asset = 0;
          *status = AssetStatus::UNLOADED;
          *file_time = 0;

          status_after_loading = AssetStatus::RELOADED;
        }

        *asset = filesystem::load(STD_HEAP, path);
        if (*asset)
        {
          *status = status_after_loading;
          *file_time = file_last_write_time;
        }
      } break;

      case assetManager::config::assetType::MAGICAVOXEL_MODEL:
      {
        ZStr path;
        {
          StrBuf::Variable *buf;
          sprintLocal(buf, label->params);
          path = buf->zStr();
        }

        if (!filesystem::exists(path))
        {
          if (AssetStatus::isLoaded[*status])
          {
            STD_HEAP->free(*asset);
            *asset = 0;
            *status = AssetStatus::UNLOADED;
            *file_time = 0;
          }
          break;
        }

        u64 file_last_write_time = filesystem::lastWriteTime(path);

        AssetStatus::Enum status_after_loading;

        if (!AssetStatus::isLoaded[*status])
        {
          status_after_loading = AssetStatus::LOADED;
        }
        else if (*file_time == file_last_write_time)
        {
          *status = AssetStatus::LOADED;
          break;
        }
        else
        {
          STD_HEAP->free(*asset);
          *asset = 0;
          *status = AssetStatus::UNLOADED;
          *file_time = 0;

          status_after_loading = AssetStatus::RELOADED;
        }

        File *vox_file_raw = filesystem::load(STD_HEAP, path);
        if (!vox_file_raw) break;
        SCOPE_EXIT(STD_HEAP->free(vox_file_raw));

        magicaVoxel::File vox_file;
        if (!vox_file.initUsingFileData(vox_file_raw->data,
                                        vox_file_raw->size))
        {
          break;
        }

        magicaVoxel::VoxelGrid *vox_grid = magicaVoxel::VoxelGrid::allocAndInitFromFile(STD_HEAP, vox_file);
        if (!vox_grid) break;
        SCOPE_EXIT(STD_HEAP->free(vox_grid));

        *asset = magicaVoxel::loadMesh::from(STD_HEAP, vox_grid, vox_file.palette);
        if (*asset)
        {
          *status = status_after_loading;
          *file_time = file_last_write_time;
        }
      } break;

      default:
        if (AssetStatus::isLoaded[*status])
        {
          STD_HEAP->free(*asset);
          *asset = 0;
          *status = AssetStatus::UNLOADED;
        }
        break;
    }
  }
}
